package MadsOp5Og6;

public class Word implements Comparable<Word>{

	private String text;
	private int amount;

	public Word(String text, int amount) {
		this.text = text;
		this.amount = amount;
	}

	public String getText() {
		return text;
	}

	public void setText(String word) {
		this.text = word;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public void Increment() {
		amount++;
	}

	@Override
	public String toString() {
		return text + " : " + amount;
	}
	
	@Override
	public int compareTo(Word word) {
		//Small to large
		//return word.getAmount() - this.amount;
		
		//Large to small
		return word.getAmount() - this.amount;
	}
	
}
