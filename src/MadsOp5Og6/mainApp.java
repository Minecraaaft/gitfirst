package MadsOp5Og6;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.TreeSet;

public class mainApp {

	public static void main(String[] args) {
		//tekstfil med navn 'Opg5' på skrivebordet
		String path = System.getProperty("user.home") + "/Desktop/" + "Opg5" + ".txt";
		
		TreeSet<String> set = readFile1(path);
		
		System.out.println("Opgave 5");
		System.out.println(set + " " + set.size());
		
		TreeMap<String,Integer> map = readFile2(path);
		
		System.out.println("Opgave 6 a");
		System.out.println(map);
		
		System.out.println("Opgave 6 b");
		
		ArrayList<Word> sorted = readFile3(path);
		Collections.sort(sorted);
		
		System.out.println(sorted);
	}

	
	public static TreeSet<String> readFile1(String path) {
		
		TreeSet<String> set = new TreeSet<String>();
		
		try {
			File file = new File(path);
			Scanner reader = new Scanner(file);
			
			while(reader.hasNextLine()) {
				set.add(reader.nextLine());
			}
			reader.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return set;
	}
	
	
	
	public static TreeMap<String,Integer> readFile2(String path) {
		
		TreeMap<String,Integer> map = new TreeMap<String,Integer>();
		
		try {
			File file = new File(path);
			Scanner reader = new Scanner(file);
			
			while(reader.hasNextLine()) {
				
				String key = reader.nextLine();
				
				if (map.containsKey(key)) {
					map.put(key, map.get(key) + 1);
				}
				else {
					map.put(key, 1);
				}
				
			}
			
			reader.close();
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	public static ArrayList<Word> readFile3(String path) {
		
		ArrayList<Word> words = new ArrayList<Word>();
		
		try {
			File file = new File(path);
			Scanner reader = new Scanner(file);
			
			while(reader.hasNextLine()) {
				
				String key = reader.nextLine();
				
				boolean found = false;
				for (Word word : words) {
					if (word.getText().equals(key)) {
						word.Increment();
						found = true;
					}
				}
				
				if (!found) {
					words.add(new Word(key, 1));
				}
				
			}
			
			reader.close();
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return words;
	}
	
}
